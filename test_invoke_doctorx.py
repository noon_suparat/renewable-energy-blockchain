import requests
import json
import time

#eliminate https certificate warning
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

'''
#global configuration start----->
sdk_host = "61.19.22.99" 
sdk_port = 8443
contract_addr = "0xf366164a44c0b71f7b0a54a2ce8e3e046ca8cf46", ###!!! smart contract address after deployment
from_wallet_addr = "0xecd6f20319f78ed0a4308c0c6367cbef95f24a48"
from_wallet_password = "123456" ###!!! Password is the one specified when using xkey to generate wallet

#loggin into the host of SDK and run the following command to generate CLIENT_ID & CLIENT_CLIENT_SECRET
#curl -XPOST https://127.0.0.1:$sdk_port/api/auth/credentials -k 
#Note that in producing environment, get_credentials should be invoked only once, once generated, never expire!!!
CLIENT_ID = "wzs1AiwLcg7OLq1FfnyV"
CLIENT_SECRET = "nHUcaCaih64eThCXfgvI95r3Z0oRV1ARKNwuUKU3"
#global configuration end <----------
'''

#global configuration start----->
sdk_host = "159.192.146.194" 
sdk_port = 8443
contract_addr = "0x393083755d7136a0a18521b52d677f2590b694e7", ###!!! smart contract address after deployment
from_wallet_addr = "0xbbeed90583e5e22e7832746cb0364380889a7bb6"
from_wallet_password = "123456" ###!!! Password is the one specified when using xkey to generate wallet

#loggin into the host of SDK and run the following command to generate CLIENT_ID & CLIENT_CLIENT_SECRET
#curl -XPOST https://127.0.0.1:$sdk_port/api/auth/credentials -k 
#Note that in producing environment, get_credentials should be invoked only once, once generated, never expire!!!
CLIENT_ID = "pYMgSw7FZCOJvenV0gMC"
CLIENT_SECRET = "uEG8WuuBk6fkOdMkV7agyyHworEqa2UeU6hywRSg"
#global configuration end <----------

"""{
  "pid": "A1M5+gmgrzk+s+pruVFPFkeByfhiMfOS6vV0Dg==",
  "personal_information": {
    "address": {
      "alley": "fHz9XQ0wwGpLRNr+L9UtehWntLkZoKlMwAHhzg==",
      "district": "0+aJMPbNc4hR2gExQH8nge8YUZNMsPDlsyrPK06NHtbeILdGAgkiAMGpgGlGGT3Orpt2V0OKWvyP6Lze",
      "full_address": "Kbccmr8DW7MKwi57oRmzy2YPX00sWLM50RGFGl8I1X81zp0yj1x9DDIjwQ3mQrZodhjfynUjY7dtqJ8Mb+q1ppMu13JtJRGMAQsbgw==",
      "house_no": "wbtvioluVo3JIzhAwl+0+cEy7qiIKEjXtFWzKg==",
      "lane": "8SGPR8sNVdqAoehQsOBhiKHHbSDIir1ZHqJKIA==",
      "nationality_id": "tC7r6/aAOxp/mje99YwRC/IXY/QjHr6jzdwnkQ==",
      "postal_code": "SMRAgjzckYo5v0PEyL5/s/nzBASMnmIZdBY8+g==",
      "provice": "ZW5yKv+6kjNEgwAsOMx4jrOvC8CPr8lV5aCs5SZHXH4vbFoO5qGO23Gij50=",
      "road": "sGUMvYpraGqRf+hA5ro17IEKwIwakSCW0gcwIQ==",
      "sub_district": "redl6hayE06p/yuyGHOR7VGkMydxiS2mMYVgr5ltcMfuacZleiMcZ7rUyGE=",
      "village_no": "wS2jR8n1PkrJDqj6O1pWAGQFMBNDZVqCsbYrDQ=="
    },
    "age": "2vK3ZQXTFuWG14vDnkFjIgAZqd+wRXSrXpO/0g==",
    "birthday": "kQcypl3oLPoVupSlsGeIdqtbmJskQK6Zrd73wg==",
    "blood_group": "d7Hd0oGbxcJNXvQYekPFHzpJcBelSvTzzRVHVg==",
    "drug_allergy": "lja+U3Ksi8IxEWTJNhd7V+tj1LZKpcA868TzLA==",
    "faceid": "UxiTN4pKq2/HfgtN5TUzdRWyVsQw6ro1deLxdw==",
    "fname": "TnXLtEne6gAc6STLJdl8lCZaGbyJaJvx18HqyQ==",
    "hn": "lr7cMTnFR3tFleVhjCKruuzd/j4S0m+W1zLdMg==",
    "home_phone": "rPiTdEsi9tn/7mRNIJq7en4+A4etxZ+9cr+xxg==",
    "hos_id": "zKWRVRpazcRab2KKQj9DddkIgqe0dESRH5lMrg==",
    "hos_name": "gn6ABMDZql6UFucYgDh2gGlzXSH/OW8nPL8lAyfUsSVD9d5j+KvguGNeUrZWC1LLXTaQxsB1k2h1PD2NBQZXRRFbfc3hjY1i24nVyuMyo7Wo+7aZ3Umj0ytNOCk=",
    "hospital_id_check": "001497200",
    "image_url": "+EoOV31EaG6NmE9AphgH6RfHJPuIEIE9S8vA3jRo4zrARZs4PmcpjG72wJlTdGh59VGm+XpCA0Bg6A+ikiyEgbH1+p8mB1f+ZB4oCdPlbFUUMTHBAmA0jVqkunL6bmdHj3m3Vsad/cb92OlHrxVG3MHwf4ueFxYeQrToiH6YyunkZLPak2exYNEM47aESHbLSDwBBpQCFisAskX8c/Zt/GrDd6/7uaVQzB1j8w==",
    "lname": "YlMa/YdV+Ux/baqy5DbhgxzG5R4TyzShV4+ih79t8GO4jMoWGf6Xy6TzyRE=",
    "phone_number": "TNO88CFm/9XhS21cuQTn+s7H2TTlRgjr3ER6Ux2sL9HrVwGyVO/bwiH0lMvlstrDgsy4ONDrrnXkZr7N",
    "pid": "A1M5+gmgrzk+s+pruVFPFkeByfhiMfOS6vV0Dg==",
    "pname": "0ACsE/nbJGqDp1ck02Mjin9RehdIwfWOZDgEpw==",
    "search_faceid": "74EQiiumQgm3LMcrhzopQSPq7iRF4c7BUUIVrg==",
    "search_pid": "1017f397007b9b789034ff7e1ae4e253cca16f1992c1da6677491defd7b5ce8b"
  },
  "visit_information": {
    "attach_search": "0ed79713ed6f647e5ca36006146fc7cec3f1b79fb39b170d27a4441b1ce4cb39",
    "bmi": "IEV2a9XCi2raifBejtB5pkcYZJIVKaf8Cs9gDA==",
    "bp": "DY3U0LFSIwjsvZXVNPXrwbZf/2dDmqe0Wtpkbg==",
    "bt": "d55NV41T+yXCS8GrTpz47r918rQOKFi1dSMWFg==",
    "bw": "N/ug316EtcrU4iYeif71BF493rAJQVl/3H2NOA==",
    "cc": "2jOoDTcTY+8eoYUoQtlqI9ZT6/gdUvVD0q0bVA==",
    "hos_id": "avEYaR04hJZ6yXVG8xV08NrcstztQ5OpHWp/ug==",
    "hos_id_check": "647cb863218d712a4f7fcc33673333e99640603d0988976a5dfec563e6fc4953",
    "hos_name": "NDTz6o4s0p581DpqvBzWx1SJLExJaNY7xBdv5Aj+enVv+TCsLCh8fCntxGVR6QvDNE61PWVcmDHZ1ZHCa3BLEFKoZfSbMDhwj4BowJ+Y3bICniR1OdvaVeTXOd9XUSnN8MwW+YoPuRHn8kfw",
    "hos_search": "17e302863a6c988298cadba8b91b1ab7acdb2d5e8d05df9702e78a066b1b183c",
    "ht": "x5baV2oT7e7COafb/1dInLjCDvAgsuaKVe4o+g==",
    "pe": "C0NlBvHYqO0uBU4EkVvctY14iFvdAyDhZK8VWQ==",
    "pi": "D5IHkXxr4MRgj7JD36J7DUglex+pTlqM/j+Jag==",
    "pid": "gHWTWKof+a5Dkf6tlv2HnubJWqdlHT093fypkw==",
    "pr": "6t3SSRPNsDqRtm/0OHB+UIoVf78a7yvWGNLJxA==",
    "rr": "WkZfcg15Yw8mD6353VccqG2mfauLG7Dib36u0A==",
    "sat_o2": "yQvtuc83qaqmgmB2e0dlCOBvqbcS7uVzXLHzTw==",
    "search": "59a3cc86bf6a3de36dd11c145302940134bb83453f7219610738cad976d35d91",
    "visit_date": "Z3WQFa4SrlnEDIuanwclzDFQxqLnKDUoiwuDDg==",
    "visit_time": "nUqH29XUTfBNaM7SFSo4rHfQK0zLmx0yDpO8pw==",
    "visit_time_check": "8c96ec9c6d02329e41314d032c5450860399467f4855bfe9d1564694d2c5fafe"
  },
  "labs_information": {
    "attach_search": "8b57001a3c82627e3f9ce7aed2000a5560913006e3acd543acf9c2359ec2b5c0",
    "hos_id": "wlkBDeOG9OFFK+oGaP7DDyYgwu9qVR9NhkrgCQ==",
    "hos_id_check": "f3dd9b364ec431841a5142f30c6cc16026def6cfa25dd71c20ea6f7211f030e0",
    "hos_name": "jkRlYeZRxkg4YzDlqBhonB7yFvEoqeejMCIkkaOr1BNDyqEfDgID7tpU4uWCL8ozAcckoPAVoFIkXb+nHqDpX1oFYwanEBj7oyIyKXonydQSbRFLS/G0uph5bRw=",
    "hos_search": "4cb5c23e57ee359e0aced464c858d53afefc818852fc8ad2b457480058d926b7",
    "lab_list": [
      {
        "detail": [
          "eDpMOmEaxS9H4xecY+KXeaFFF3c9Ck0EhQj6DtPY6Sde6S7QdUM9/7fce4qhlleWx0e9LyW5cf37zZYe",
          "eDpMOmEaxS9H4xecY+KXeaFFF3c9Ck0EhQj6DtPY6Sde6S7QdUM9/7fce4qhlleWx0e9LyW5cf37zZYe",
          "eDpMOmEaxS9H4xecY+KXeaFFF3c9Ck0EhQj6DtPY6Sde6S7QdUM9/7fce4qhlleWx0e9LyW5cf37zZYe"
        ],
        "ordername": "f6Ey4OPhxZdKMgTZYKY8dX1YindBV7jPKe6v+7YYtfkfu5Xs80pvnL6frfU=",
        "reporter_name": "SzpuNWaGpzLXMYOR13joB1gV5/LBtG/h/wQ0L7JTaFhLag8x+0mYmhR0ONBIHQB44FfUMQUvErnEH/GMh9qQuT4iZgTGLfSLRRz8ug=="
      }
    ],
    "pid": "/UCCAl+DpG65vk5CRV3keNkBz0rhDsL3hqjzCQ==",
    "search": "bb1b811edbe578bd70dc6eafa17e589eb94ab1f0b764f2f6158ae73b7eb31f25",
    "visit_date": "7M2pB5t6GdQZv80MxtrAPT8o2G3anqgrDr3dcg==",
    "visit_time": "miVn4HWlj+b4O1FhIlMRK8k/Bm/sLhm3NNrtHw==",
    "visit_time_check": "81bd69dacada4fc5b0ebe11bb43768e528ade2e16e411f4f02ba9bfe4b251c65"
  },
  "diagnosis_information": {
    "attach_search": "0ed79713ed6f647e5ca36006146fc7cec3f1b79fb39b170d27a4441b1ce4cb39",
    "diagnosis": [
      {
        "diag": "eX1A0oi2wNQctx7Di8917GY+rg26u6cEA1isEd8xzbz666VD+fBzOiL8SwU575mUnOA/KmOFtJfJ9Uy0",
        "diagnosis_date": "2R6qzZrFFd6dd5KNmqPNexF2BGCnIBUV9U4vBw=="
      },
      {
        "diag": "eX1A0oi2wNQctx7Di8917GY+rg26u6cEA1isEd8xzbz666VD+fBzOiL8SwU575mUnOA/KmOFtJfJ9Uy0",
        "diagnosis_date": "2R6qzZrFFd6dd5KNmqPNexF2BGCnIBUV9U4vBw=="
      }
    ],
    "hos_id": "Dax0LLTlPe89APxOWljcqHKfWT9Iw0ACdkaAuA==",
    "hos_id_check": "647cb863218d712a4f7fcc33673333e99640603d0988976a5dfec563e6fc4953",
    "hos_name": "Cmp2Yc7hfD7n8vdZTc/nEsX2vCQKwVJyQc2kQYlpHGYQZSLM6d5H5JVNaZtP2LIAV8H8aIDup9nN4j9GT0IrkihoGtCswE4F8hewFfBvXdGnwC6TRiiN1zBstHNaSKZFDT4qoc0N+ofWsowW",
    "hos_search": "17e302863a6c988298cadba8b91b1ab7acdb2d5e8d05df9702e78a066b1b183c",
    "pid": "c2ys9QVhp6umYTFuNJHfWJ7g9Y7J35UU8PDFBQ==",
    "search": "59a3cc86bf6a3de36dd11c145302940134bb83453f7219610738cad976d35d91",
    "visit_date": "2SUpShhn9asS0B7/l5Sv8Ds1S0G7ezhgPHhj6g==",
    "visit_time": "StixRBY1vk7p1wccbW7PBTwT0/K+OO2Xa8/d7A==",
    "visit_time_check": "8c96ec9c6d02329e41314d032c5450860399467f4855bfe9d1564694d2c5fafe"
  },
  "order_information": {
    "attach_search": "b9645f0eaa86ef1d9c2695d4db3d22be07ae4904ebd71e3e5e78ac33b010347e",
    "hos_id": "RPMm1YmLCUhujV0sPcKtFJKU8/g87zpCqiYutQ==",
    "hos_id_check": "f3dd9b364ec431841a5142f30c6cc16026def6cfa25dd71c20ea6f7211f030e0",
    "hos_name": "kWUqlklnJIzfy9J++Ghx20pyJs1fB0AnnXVEqpjamneNj7pPndj4GPA9pSdwv9yIf8iKLbL9ljahmpfiA3aoQzzVlBMuxyVlFfQCWHnClCtr/+8VEMLAZVMiwK8=",
    "hos_search": "2482dc411486c6bade57bb72278a2764b9161c00618c0dbd96dc9eaa381c4ab0",
    "order_list": [
      {
        "med": "hpiVWNc1GieRbN3qzGv5bhAX4KQduGC4kchWAfXv+pu0MmR3GolLFnL7LvwiD3DH3jMUJbYy7H3k8GgWhLGG1FACZfiC/rIeXrWz8Q==",
        "verify_date": "+iGSwk+847WyyOOfShqpxVjNNMs+akeAiDPsTA=="
      },
      {
        "med": "cmRMaKQ3dtyzPNgBKmGlFxamq9VBj2kqhDAxEmJlGC5JU5arXlXj/FeCgvu+5/ipHrUH+FqFfPwMaKjU",
        "verify_date": "EIVgpMMkHXruwiIBEHDavNb8BEweK4iPOqtqPA=="
      },
      {
        "med": "cmRMaKQ3dtyzPNgBKmGlFxamq9VBj2kqhDAxEmJlGC5JU5arXlXj/FeCgvu+5/ipHrUH+FqFfPwMaKjU",
        "verify_date": "EIVgpMMkHXruwiIBEHDavNb8BEweK4iPOqtqPA=="
      }
    ],
    "pid": "zMjoMdgUYfONqlOCOVKasrZlvRuwbdM07kPt9A==",
    "search": "68cbdf439fd0b8f1e519f13e885327303cd4a1bb768c364dc72ba143d19f774f",
    "visit_date": "dRZrj72TO0SdjIgu1kbcqudVIT+5p/43ncNj7w==",
    "visit_time": "R/zGM8JscEyL8VPeSj9heab6aamOP067vidqtw==",
    "visit_time_check": "f9811d192dc8cbfa6462bc5d4f3438c4ef5f887ef0aeff2673ec61266eb4963b"
  }
}
"""

#return: status_code, json_object
def post2sdk(url, token, data_object):
    data = None
    if data_object != None:
      data = json.dumps(data_object)
    headers = None
    if token != None and len(token) > 0:
        headers = {"Authorization": "Bearer %s"%token}
    resp = requests.post(url, headers = headers, data = data, verify=False)
    if resp.ok:
      resp_json = json.loads(resp.text)
      return resp.status_code, resp_json
    else:
      return resp.status_code, None

#return: error_msg, token
def refresh_token(client_id, client_secret):
  url = "https://%s:%d/api/auth/token?grant_type=client_credentials&client_id=%s&client_secret=%s"%(sdk_host, sdk_port, client_id, client_secret)
  status_code, json_object = post2sdk(url, None, None)
  if status_code != 200:
    return "http status code:%d"%status_code, None, None
  if json_object["code"] != 0:
    return json_object["msg"], None, None
  return "", json_object["data"]["access_token"]

#write patient data into the blockchain
#return: error_msg, transaction_hash, pid
def write_patient_data(host, port, token, patient_object):
  url = "https://%s:%d/api/node/contract/invoke"%(host, port)
  #refer to manual http://159.192.146.194:8081/docs/en/%E5%BC%80%E5%8F%91%E8%80%85%E6%89%8B%E5%86%8C/sdk.html
  data_object = {
    "from": "%s"%from_wallet_addr,
    "password": "%s"%from_wallet_password,  
    "contract": {
      "address": "%s"%contract_addr, #!!! http://159.192.146.194:8081/#/contract-manage/detail/2
      "method": "create",
      "params": []
    }
  }

  params = data_object["contract"]["params"]
  params.append(patient_object["pid"])
  params.append(json.dumps(patient_object["personal_information"]))
  params.append(json.dumps(patient_object["diagnosis_information"]))
  params.append(json.dumps(patient_object["labs_information"]))
  params.append(json.dumps(patient_object["order_information"]))
  params.append(json.dumps(patient_object["visit_information"]))

  status_code, json_object = post2sdk(url, token, data_object)
  if status_code != 200:
    return "http status code:%d"%status_code, None, None
  if json_object["code"] != 0:
    return json_object["msg"], None, None
  return "", json_object["data"]["hash"], patient_object["pid"]

#return: error_msg, blocknumber(if appended in the blockchain). A valid `blocknumber` indicates that 
#the transaction is appended in the blockchain. It's worth noting that transaction included in the blockchain doesn't mean
#it will be executed without an error, in the contrast, included in the blockchain only means it will be executed.
def query_transaction_status(host, port, token, hash):
  url = "https://%s:%d/api/node/transaction/detail"%(host, port)
  data_object = {
    "address":"%s"%contract_addr,
    "hash":"%s"%hash
  }
  status_code, json_object = post2sdk(url, token, data_object)
  if status_code != 200:
    return "http status code:%d"%status_code, None
  if json_object["code"] != 0:
    return json_object["msg"], None
  return "", json_object["data"]["blockNumber"]
  
#return: error_msg. Empty error_msg means transaction is executed successfully.
def query_transaction_result(host, port, token, hash):
  url = "https://%s:%d/api/node/transaction/receipt"%(host, port)
  data_object = {
    "address":"%s"%contract_addr,
    "hash":"%s"%hash
  }
  status_code, json_object = post2sdk(url, token, data_object)
  if status_code != 200:
    return "http status code:%d"%status_code
  if json_object["code"] != 0:
    return json_object["msg"]
  if json_object["data"]["code"] != 0:
    return json_object["data"]["result"]
  return ""

#return error_msg, data_str.
def read_patient_data(host, port, token, pid):
  url = "https://%s:%d/api/node/contract/call"%(host, port)
  data_object = {
    "from":"%s"%from_wallet_addr,
    "contract":{
      "address":"%s"%contract_addr,
      "method":"getPatientInfoArray",
      "params":["%s"%pid]
    }
  }
  status_code, json_object = post2sdk(url, token, data_object)
  if status_code != 200:
    return "http status code:%d"%status_code, ""
  if json_object["code"] != 0:
    return json_object["msg"], ""
  return "", json.dumps(json_object["data"]["result"])

def main():
  error_msg = ""
  print("Global configuration:")
  print("sdk_host = %s"%sdk_host)
  print("dk_port = %d"%sdk_port)
  print("contract_addr = %s"%contract_addr)
  print("from_wallet_addr = %s"%from_wallet_addr)
  print("from_wallet_password = %s"%from_wallet_addr)
  
  if sdk_host == "" or sdk_port == 0 or contract_addr == "" or from_wallet_addr == "" or from_wallet_addr == "":
    print("\nPlease fix global configuration and return again.\n")
    return

  sample_patient_data = """
    {
    "pid": "A1M5+gmgrzk+s+pruVFPFkeByfhiMfOS6vV0Dg==",
    "personal_information": {
      "address": {
        ...
      },
      "age": "2vK3ZQXTFuWG14vDnkFjIgAZqd+wRXSrXpO/0g==",
      ...
    },
    "visit_information": {
      "attach_search": "0ed79713ed6f647e5ca36006146fc7cec3f1b79fb39b170d27a4441b1ce4cb39",
      "bmi": "IEV2a9XCi2raifBejtB5pkcYZJIVKaf8Cs9gDA==",
      ...
    },
    "labs_information": {
      "attach_search": "8b57001a3c82627e3f9ce7aed2000a5560913006e3acd543acf9c2359ec2b5c0",
      "hos_id": "wlkBDeOG9OFFK+oGaP7DDyYgwu9qVR9NhkrgCQ==",
      ...
    },
    "diagnosis_information": {
      "attach_search": "0ed79713ed6f647e5ca36006146fc7cec3f1b79fb39b170d27a4441b1ce4cb39",
      "diagnosis": [
        ...
      ],
      ...
    },
    "order_information": {
      "attach_search": "b9645f0eaa86ef1d9c2695d4db3d22be07ae4904ebd71e3e5e78ac33b010347e",
      "visit_time_check": "f9811d192dc8cbfa6462bc5d4f3438c4ef5f887ef0aeff2673ec61266eb4963b"
      ...
    }
  }
  """
  print("\nPlease input a file name which include a json string:\n The string is in the form of:\n%s"%sample_patient_data)
  file_name = raw_input("file_name:")
  print("file_name:%s"%file_name)
  patient_object = json.loads("{}")
  try:
    file_object = open(file_name, "r")
    json_str = file_object.read()
    patient_object = json.loads(json_str)
  except Exception as err:
    print("read json_str error:%s"%err)
    return
  print("file %s loaded."%file_name)

  print("\nStart to get token... Note that in producing environment, refresh token should only be invoked periodically rather than every time before read/write data!!!")
  error_msg, token = refresh_token(CLIENT_ID, CLIENT_SECRET)
  if error_msg != "":
    print("refresh_token return:%s"%error_msg)
    return
  print("Token generated: %s"%token)

  print("\nStart to write patient data to blockchain...")
  error_msg, transaction_hash, pid = write_patient_data(sdk_host, sdk_port, token, patient_object)
  if error_msg != "":
    print("write_patient_data return:%s"%error_msg)
    return
  print("Write data return OK, transaction_hash:%s pid:%s"%(transaction_hash, pid))

  print("\n")
  while True :
    print("Waiting for the transaction to be included in blockchain...")
    error_msg, blocknumber = query_transaction_status(sdk_host, sdk_port, token, transaction_hash)
    if error_msg != "":
      if error_msg == "tx not exist":
        continue
      print("query_transaction_status return:%s"%error_msg)
      return
    if blocknumber != 0:
      print("transaction included in block:%d."%blocknumber)
      break
    time.sleep(2)
    

  print("\n")
  error_msg = query_transaction_result(sdk_host, sdk_port, token, transaction_hash)
  if error_msg != "":
    print("query_transaction_status return:%s."%error_msg)
    return
  else:
    print("transaction execute result:success.")
  
  print("\n")
  error_msg, data_str = read_patient_data(sdk_host, sdk_port, token, pid)
  if error_msg != "":
    print("read_patient_data return:%s."%error_msg)
    return
  print("data_str:\n%s"%data_str)

if __name__ == '__main__':
  main()

  